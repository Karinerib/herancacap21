public class Retangulo extends OutrasFigurasGeometricas{


	double base;
	double altura;
	double area;

	public double calcularArea(double base, double altura){
		area = base * altura;
		return area;
	}
}
