public class Quadrado extends OutrasFigurasGeometricas{

	int valorDoLado;
	double perimetro;
	double area;
	
	public double calcularPerimetro(double valorDoLado){
	perimetro = valorDoLado * 4;
	return perimetro;
}
	public double calcularArea(double valorDoLado){
	area = valorDoLado * valorDoLado;
	return area;
	
}

}
